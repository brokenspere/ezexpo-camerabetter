/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import PhotoUpload from './app/src/features/upload/PhotoUpload';

AppRegistry.registerComponent(appName, () => PhotoUpload);
