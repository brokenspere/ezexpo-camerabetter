/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import axios from 'axios';
import _ from 'lodash';
import * as RNFS from 'react-native-fs';
import {dirPictures} from './DirConstants';

const moveAttachment = async (filePath, newFilePath) => {
  return new Promise((resolve, reject) => {
    RNFS.mkdir(dirPictures)
      .then(() => {
        RNFS.moveFile(filePath, newFilePath)
          .then(() => {
            console.log('FILE MOVED', filePath, newFilePath);
            resolve(true);
          })
          .catch(error => {
            console.log('moveFile error', error);
            reject(error);
          });
      })
      .catch(err => {
        console.log('mkdir error', err);
        reject(err);
      });
  });
};

class PhotoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      boothId: '',
      emotion: [],
    };
  }

  uploadEmotion(val) {
    console.log('in upEmo');
    axios({
      url: `http://10.80.124.27:8080/addVisitorEmotion?id=${this.state.boothId}`,
      method: 'post',
      data: {
        happy: val.happy,
        sad: val.sad,
        surprise: val.surprise,
        angry: val.angry,
        disgust: val.disgust,
        fear: val.fear,
        netural: val.neutral,
        total: val.total,
        visitTime: val.visitTime,
      },
    })
      .then(res => {
        alert('Upload sucess');
        console.log('success', res.data);
      })
      .catch(err => {
        console.warn('upload error', JSON.stringify(err, null, 4));
        alert('Cannot upload');
        return;
      });
  }

  upload = async () => {
    if (_.isEmpty(this.state.boothId)) {
      alert('Please input booth id');
      return;
    }
    await RNFS.readDir(dirPictures)
      .then(files => {
        if (_.isEmpty(files)) {
          alert('No photo found');
          return;
        }
        const form = new FormData();
        _.forEach(files, val => {
          form.append('file', {
            uri: 'file://' + val.path,
            type: 'image/jpeg',
            name: val.name,
          });
        });
        console.log(form);
        axios
          .post('https://flaskdetect33.herokuapp.com/upload', form)
          .then(response => {
            console.log(response.data);
            this.setState({emotion: response.data});
            _.forEach(response.data, async val => {
              await this.uploadEmotion(val);
            });
          })
          .then(() => {
            _.forEach(files, val => {
              RNFS.unlink(val.path)
                .then(() => {
                  console.log('FILE DELETED', val.path);
                })
                // `unlink` will throw an error, if the item to unlink does not exist
                .catch(err => {
                  console.log(err.message);
                });
            });
          })
          .catch(err => {
            console.log(err, 'error');
            alert('Cannot upload');
            return;
          });
      })
      .catch(err => {
        console.warn(err);
      });
  };

  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5};
      await this.camera.takePictureAsync(options).then(data => {
        this.saveImage(data.uri);
      });
    }
  };
  saveImage = async filePath => {
    try {
      const date = Math.floor(Date.now() / 10);
      const newFilePath = `${dirPictures}/${date}0.jpg`;
      const imageMoved = await moveAttachment(filePath, newFilePath);
      console.log('image moved', imageMoved);
    } catch (error) {
      console.log(error);
    }
  };
  startTakePhoto = () => {
    this.timer = setInterval(() => this.takePicture(), 5000);
  };
  stopTakePhoto = () => {
    clearInterval(this.timer);
  };
  onIdChange = text => {
    this.setState({boothId: text});
  };

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.off}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          />
          <View
            style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              onChangeText={this.onIdChange.bind(this)}
              value={this.state.boothId}
            />
            <TouchableOpacity onPress={this.setBoothId} style={styles.capture}>
              <Text style={{fontSize: 14}}> set id </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
            <TouchableOpacity
              onPress={this.startTakePhoto.bind(this)}
              style={styles.capture}>
              <Text style={{fontSize: 14}}> START </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.stopTakePhoto}
              style={styles.capture}>
              <Text style={{fontSize: 14}}> STOP </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.upload.bind(this)}
              style={styles.capture}>
              <Text style={{fontSize: 14}}> Upload </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 5,
  },
  input: {
    flex: 0,
    margin: 20,
    height: 40,
    width: 200,
    color: 'white',
    borderColor: 'white',
    alignSelf: 'center',
    borderWidth: 1,
  },
});

export default PhotoUpload;
